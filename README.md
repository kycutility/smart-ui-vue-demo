# smart-ui-vue-demo

## Requirements

Smart UI Vue Demo requires the following requirements to run:

- [Node.js](https://nodejs.org/) 16.x LTS

## Project setup

```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```